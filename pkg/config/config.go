package config

import (
	"os"

	"gopkg.in/yaml.v3"
)

type Config struct {
	Schema string
	Sqlc   bool
}

func Load() (conf *Config, err error) {
	f, err := os.Open("dripple.yml")
	if err != nil {
		if os.IsNotExist(err) {
			return nil, nil
		}
		return nil, err
	}
	defer f.Close()
	err = yaml.NewDecoder(f).Decode(&conf)
	if err != nil {
		return nil, err
	}

	return
}
