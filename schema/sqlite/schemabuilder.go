package sqlite

import (
	"fmt"
	"strings"

	"gitlab.com/codingpaws/dripple/schema"
)

type FieldDef interface {
	PrimaryKey() FieldDef
	Index(name ...string) FieldDef
	// References(table, key string) FieldDef
	Nullable() FieldDef
	Unique() FieldDef
	UniqueIndex(name ...string) FieldDef
	Default(value string) FieldDef
	sqlitedef() fieldDef
}

type fieldReference struct {
	Table string `json:"table"`
	Key   string `json:"key"`
}

type fieldDef struct {
	typ  string
	name string

	isPrimary, isUnique, isNullable bool
	defaultValue                    string

	// note: this is empty as soon as the def is
	// added to a table, to ensure compareability
	indicies       []string
	uniqueIndicies []string
	// References []fieldReference
}

func (self fieldDef) ShortString() string {
	attrs := []string{}

	if self.isPrimary {
		attrs = append(attrs, "primary")
	}
	if self.defaultValue != "" {
		attrs = append(attrs, fmt.Sprintf("default=%s", self.defaultValue))
	}
	if self.isUnique {
		attrs = append(attrs, "unique")
	}
	if self.isNullable {
		attrs = append(attrs, "nullable")
	}

	return fmt.Sprintf("%s[%s]", self.typ, strings.Join(attrs, ","))
}

func (self fieldDef) ToSQLDefinition() (string, error) {
	subparts := []string{
		escapeName(self.name),
		self.typ,
	}

	if self.isPrimary {
		subparts = append(subparts, "PRIMARY KEY")
	}

	if self.defaultValue != "" {
		subparts = append(subparts, fmt.Sprintf("DEFAULT %s", self.defaultValue))
	}

	if self.isUnique {
		if self.isPrimary {
			return "", fmt.Errorf("field %s can’t be primary key and unique, it is unique by default", self.name)
		}
		subparts = append(subparts, "UNIQUE")
	}

	if !self.isNullable {
		subparts = append(subparts, "NOT NULL")
	}

	return strings.Join(subparts, " "), nil
}

func (self fieldDef) sqlitedef() fieldDef { return self }

//	func (self fieldDef) References(table, key string) FieldDef {
//		self.references = append(self.references, fieldReference{
//			table: table,
//			key:   key,
//		})
//		return self
//	}

func (self fieldDef) UniqueIndex(name ...string) FieldDef {
	self.uniqueIndicies = append(self.uniqueIndicies, name...)
	return self
}

func (self fieldDef) Nullable() FieldDef {
	self.isNullable = true
	return self
}

func (self fieldDef) Index(name ...string) FieldDef {
	self.indicies = append(self.indicies, name...)
	return self
}

func (self fieldDef) Default(value string) FieldDef {
	self.defaultValue = value
	return self
}

func (self fieldDef) PrimaryKey() FieldDef {
	self.isPrimary = true
	return self
}

func (self fieldDef) Unique() FieldDef {
	self.isUnique = true
	return self
}

type table struct {
	name           string
	indicies       map[string][]string
	uniqueIndicies map[string][]string
	fields         map[string]fieldDef

	fieldOrder []string
}

// func (self table) HasDifference(other compare.HasDifference) bool {
// 	otherTable := other.(table)

// 	if self.name != otherTable.name {
// 		return true
// 	}

// 	return compare.IsDifferent(self.fields, otherTable.fields)
// }

func Table(name string, fields ...FieldDef) schema.TableDef {
	defs := map[string]fieldDef{}
	indicies := make(map[string][]string)
	uniqueIndicies := make(map[string][]string)
	order := []string{}

	for _, f := range fields {
		def := f.sqlitedef()
		order = append(order, def.name)

		for _, indexName := range def.indicies {
			// indicies are global in sqlite (wtf???) so we
			// scope them per table like I’d expected them to work
			indexName = fmt.Sprintf("idx__%s__%s", name, indexName)

			indicies[indexName] = append(indicies[indexName], def.name)
		}

		for _, indexName := range def.uniqueIndicies {
			// see wtf above
			indexName = fmt.Sprintf("uidx__%s__%s", name, indexName)

			uniqueIndicies[indexName] = append(uniqueIndicies[indexName], def.name)
		}

		defs[def.name] = def
	}

	return &table{
		name:           name,
		fields:         defs,
		indicies:       indicies,
		uniqueIndicies: uniqueIndicies,
		fieldOrder:     order,
	}
}

func Integer(name string) FieldDef {
	return fieldDef{
		typ:  "INTEGER",
		name: name,
	}
}

func Float(name string) FieldDef {
	return fieldDef{
		typ:  "REAL",
		name: name,
	}
}

func String(name string) FieldDef {
	return fieldDef{
		typ:  "TEXT",
		name: name,
	}
}

func Blob(name string) FieldDef {
	return fieldDef{
		typ:  "BLOB",
		name: name,
	}
}

func Datetime(name string) FieldDef {
	return fieldDef{
		typ:  "DATETIME",
		name: name,
	}
}

func Bool(name string) FieldDef {
	return fieldDef{
		typ:  "BOOLEAN",
		name: name,
	}
}
