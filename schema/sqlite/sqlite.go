package sqlite

import (
	"fmt"
	"strings"
)

type index struct {
	Name   string
	Table  string
	Fields []string
	Unique bool
}

func (self index) ToSQL() string {
	renderedFields := []string{}
	for _, field := range self.Fields {
		renderedFields = append(renderedFields, escapeName(field))
	}

	var unique string
	if self.Unique {
		unique = "UNIQUE "
	}

	return fmt.Sprintf(
		"CREATE %sINDEX %s ON %s (%s);",
		unique,
		escapeName(self.Name),
		escapeName(self.Table),
		strings.Join(renderedFields, ", "),
	)
}

func (self table) resolveIndicies() map[string]index {
	indicies := map[string]index{}
	for name, fields := range self.indicies {
		indicies[name] = index{
			Name:   name,
			Table:  self.name,
			Fields: fields,
		}
	}

	for name, fields := range self.uniqueIndicies {
		indicies[name] = index{
			Name:   name,
			Table:  self.name,
			Fields: fields,
			Unique: true,
		}
	}

	return indicies
}

func (self table) Name() string {
	return self.name
}

func (self *table) ToSQL() ([]string, error) {
	if len(self.fields) == 0 {
		return nil, fmt.Errorf("table %s must have at least 1 field", escapeName(self.name))
	}
	defsByName := map[string]fieldDef{}

	for _, field := range self.fields {
		_, ok := defsByName[field.name]
		if ok {
			return nil, fmt.Errorf("duplicate field %s.%s", escapeName(self.name), escapeName(field.name))
		}
		defsByName[field.name] = field
	}

	fields := []string{}

	for _, fieldName := range self.fieldOrder {
		field := self.fields[fieldName]
		def, err := field.ToSQLDefinition()
		if err != nil {
			return nil, fmt.Errorf("table %s: %w", self.name, err)
		}
		fields = append(fields, def)
	}

	var indiciesQuery []string
	for _, index := range self.resolveIndicies() {
		indiciesQuery = append(indiciesQuery, index.ToSQL())
	}

	tableQuery := fmt.Sprintf(
		"CREATE TABLE %s (\n%s\n);\n",
		escapeName(self.name),
		"  "+strings.Join(fields, ",\n  "),
	)

	queries := []string{
		tableQuery,
	}
	queries = append(queries, indiciesQuery...)

	return queries, nil
}
