package sqlite

import "strings"

func escapeName(name string) string {
	return strings.NewReplacer(
		"`", "",
		"\n", "",
		"\r", "",
		":", "",
		`"`, "",
		`'`, "",
		`:`, "",
		`+`, "",
		`-`, "",
	).Replace(name)
}
