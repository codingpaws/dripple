package sqlite

import (
	"bytes"
	"fmt"
	"os/exec"
	"slices"
	"strings"
)

var schemaTypes = map[string]string{
	"INTEGER": "Integer",
	"REAL":    "Float",
	"TEXT":    "String",
}

func (self Schema) ToGo() (string, error) {
	cmd := exec.Command("gofmt")

	in := new(bytes.Buffer)
	out := new(bytes.Buffer)
	outErr := new(bytes.Buffer)
	cmd.Stdin = in
	cmd.Stdout = out
	cmd.Stderr = outErr

	_, _ = fmt.Fprintln(in, "package main")
	_, _ = fmt.Fprintln(in, `import "gitlab.com/codingpaws/dripple/schema"`)
	_, _ = fmt.Fprintln(in, `import "gitlab.com/codingpaws/dripple/schema/sqlite"`)

	for _, table := range self.tables {
		_, _ = fmt.Fprintf(in, "var %s = sqlite.Table(\n\"%s\",\n", table.name, table.name)

		for _, field := range table.fields {
			goTyp, ok := schemaTypes[field.typ]
			if !ok {
				return "", fmt.Errorf("unknown type %s for field %s.%s", field.typ, table.name, field.name)
			}

			_, _ = fmt.Fprintf(in, "sqlite.%s(\"%s\")", goTyp, field.name)
			if field.isPrimary {
				_, _ = fmt.Fprint(in, ".PrimaryKey()")
			}
			if field.defaultValue != "" {
				_, _ = fmt.Fprintf(in, ".Default(`%s`)", field.defaultValue)
			}
			if field.isNullable {
				_, _ = fmt.Fprint(in, ".Nullable()")
			}
			if field.isUnique {
				_, _ = fmt.Fprint(in, ".Unique()")
			}
			for indexName, fields := range table.indicies {
				if slices.Contains(fields, field.name) {
					indexName = strings.TrimPrefix(indexName, fmt.Sprintf("idx__%s__", table.name))
					_, _ = fmt.Fprintf(in, ".Index(\"%s\")", indexName)
				}
			}
			_, _ = fmt.Fprintln(in, ",")
		}
		_, _ = fmt.Fprintln(in, ")")
		_, _ = fmt.Fprintln(in)
	}

	_, _ = fmt.Fprintln(in, "func main() {")
	_, _ = fmt.Fprintln(in, "	schema.Emit(")
	for _, table := range self.tables {
		_, _ = fmt.Fprintf(in, "%s,\n", table.name)
	}
	_, _ = fmt.Fprintln(in, "	)")
	_, _ = fmt.Fprintln(in, "}")

	err := cmd.Run()
	if err != nil {
		fmt.Println(outErr)
		return "", err
	}

	return out.String(), nil
}
