package sqlite

import (
	"fmt"
	"io"
	"slices"
	"strings"

	sql "github.com/KevSlashNull/rqlite_sql"
	"github.com/fatih/color"
	"gitlab.com/codingpaws/dripple/internal/compare"
	"gitlab.com/codingpaws/dripple/schema"
)

type Schema struct {
	tables map[string]table
}

type Resolution struct {
	order schema.ResolutionOrder

	Description string
	Queries     []string
	Type        schema.ResolutionType
	Destructive bool
}

var yellow = color.YellowString

func (expected *Schema) CompareAgainstDatabaseSchema(database *Schema) (resolutions []Resolution, err error) {
	if database == nil {
		return nil, fmt.Errorf("database schema is null")
	}

	addedTables, deletedTables, changedTables := compare.CompareMap(database.tables, expected.tables)
	lateResolutions := []Resolution{}

	for _, name := range addedTables {
		table := expected.tables[name]
		sql, err := table.ToSQL()
		if err != nil {
			return nil, fmt.Errorf("generating SQL for creating table %s: %w", name, err)
		}

		createSql := sql[0]
		indiciesEtcSql := sql[1:]

		resolutions = append(resolutions, Resolution{
			Description: fmt.Sprintf("table %s", yellow(name)),
			Queries:     []string{createSql},
			Type:        schema.ResolutionCreate,
			order:       schema.OrderTableCreate,
		})

		lateResolutions = append(lateResolutions, Resolution{
			Description: fmt.Sprintf("indicies & constraints for %s", yellow(name)),
			Queries:     indiciesEtcSql,
			Type:        schema.ResolutionCreate,
			order:       schema.OrderTableAfterCreate,
		})
	}
	resolutions = append(resolutions, lateResolutions...)

	for _, name := range deletedTables {
		resolutions = append(resolutions, Resolution{
			Description: fmt.Sprintf("table %s", yellow(name)),
			Queries:     []string{fmt.Sprintf("drop table %s", escapeName(name))},
			Type:        schema.ResolutionDelete,
			order:       schema.OrderTableDrop,
			Destructive: true,
		})
	}

	for _, name := range changedTables {
		newTable := expected.tables[name]
		addedFields, deletedFields, changedFields := compare.CompareMap(database.tables[name].fields, newTable.fields)
		for _, fieldName := range addedFields {
			def, err := newTable.fields[fieldName].ToSQLDefinition()
			if err != nil {
				return nil, fmt.Errorf("table %s: %w", name, err)
			}

			query := fmt.Sprintf("ALTER TABLE %s\n  ADD COLUMN %s", name, def)

			resolutions = append(resolutions, Resolution{
				Description: fmt.Sprintf("add column %s.%s", yellow(name), yellow(fieldName)),
				Queries:     []string{query},
				Type:        schema.ResolutionCreate,
				order:       schema.OrderColumnCreate,
			})
		}

		for _, fieldName := range deletedFields {
			query := fmt.Sprintf("ALTER TABLE %s\n  DROP COLUMN %s", name, fieldName)

			resolutions = append(resolutions, Resolution{
				Description: fmt.Sprintf("drop column %s.%s", yellow(name), yellow(fieldName)),
				Queries:     []string{query},
				Type:        schema.ResolutionDelete,
				Destructive: true,
				order:       schema.OrderColumnDrop,
			})
		}

		for _, fieldName := range changedFields {
			fromDef := database.tables[name].fields[fieldName].ShortString()
			toDef := expected.tables[name].fields[fieldName].ShortString()
			return nil, fmt.Errorf(
				"field %s.%s has changed (from %s to %s), this is still unsupported! please change it manually 🤞",
				yellow(name),
				yellow(fieldName),
				yellow(fromDef),
				yellow(toDef),
			)
		}

		exisitingIndicies := database.tables[name].resolveIndicies()
		newerIndicies := newTable.resolveIndicies()
		addedIndicies, deletedIndicies, changedIndicies := compare.CompareMap(exisitingIndicies, newerIndicies)

		for _, indexName := range deletedIndicies {
			query := fmt.Sprintf("DROP INDEX %s", indexName)
			resolutions = append(resolutions, Resolution{
				Description: fmt.Sprintf("drop index %s", yellow(indexName)),
				Queries:     []string{query},
				Type:        schema.ResolutionDelete,
				order:       schema.OrderIndexDrop,
			})
		}

		for _, indexName := range addedIndicies {
			index := newerIndicies[indexName]
			query := index.ToSQL()

			resolutions = append(resolutions, Resolution{
				Description: fmt.Sprintf(
					"add index %s on %s.(%s)",
					yellow(indexName),
					yellow(name),
					yellow(strings.Join(index.Fields, ", ")),
				),
				Queries: []string{query},
				Type:    schema.ResolutionCreate,
				order:   schema.OrderIndexCreate,
			})
		}

		for _, indexName := range changedIndicies {
			index := exisitingIndicies[indexName]
			dropQuery := fmt.Sprintf("DROP INDEX %s", indexName)
			createQuery := newerIndicies[indexName].ToSQL()

			resolutions = append(resolutions, Resolution{
				Description: fmt.Sprintf(
					"update index %s on %s.(%s) to (%s)",
					yellow(indexName),
					yellow(name),
					yellow(strings.Join(exisitingIndicies[indexName].Fields, ",")),
					yellow(strings.Join(index.Fields, ",")),
				),
				Queries: []string{dropQuery, createQuery},
				Type:    schema.ResolutionCreate,
				order:   schema.OrderIndexUpdate,
			})
		}
	}

	for _, resolution := range resolutions {
		if resolution.order == 0 {
			panic(fmt.Errorf("resolution '%s' must have order", resolution.Description))
		}
	}

	slices.SortFunc(resolutions, func(a, b Resolution) int {
		return int(a.order - b.order)
	})

	return
}

func FromSchema(schemaReader io.Reader) (*Schema, error) {
	tables := map[string]table{}

	parser := sql.NewParser(schemaReader)
	for {
		s, err := parser.ParseStatement()
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}

		switch stmt := s.(type) {
		case *sql.CreateTableStatement:
			tbl := tables[stmt.Name.Name]

			tbl.name = stmt.Name.Name
			tbl.fields = make(map[string]fieldDef)
			tbl.indicies = make(map[string][]string)
			tbl.uniqueIndicies = make(map[string][]string)

			for i, col := range stmt.Columns {
				if col.Type == nil {
					return nil, fmt.Errorf("invalid type on %s, col #%d", stmt.Name.Name, i)
				}
				tbl.fieldOrder = append(tbl.fieldOrder, col.Name.Name)
				def := fieldDef{
					typ:        normalizeType(col.Type.String()),
					name:       col.Name.Name,
					isNullable: true, // needs explicit NOT NULL
				}

				for _, constr := range col.Constraints {
					switch constr := constr.(type) {
					case *sql.UniqueConstraint:
						def.isUnique = true
					case *sql.NotNullConstraint:
						def.isNullable = false
					case *sql.PrimaryKeyConstraint:
						def.isPrimary = true
					case *sql.DefaultConstraint:
						def.defaultValue = constr.String()[len("DEFAULT "):]
					default:
						return nil, fmt.Errorf("%s.%s: unsupported constraint: %s", tbl.name, col.Name, constr)
					}
				}

				tbl.fields[def.name] = def
			}
			tables[stmt.Name.Name] = tbl
		case *sql.CreateIndexStatement:
			tbl, ok := tables[stmt.Table.Name]
			if !ok {
				return nil, fmt.Errorf("index %s: unknown relation %s", stmt.Name.Name, stmt.Table.Name)
			}
			for _, col := range stmt.Columns {
				ident, ok := col.X.(*sql.Ident)
				if !ok {
					return nil, fmt.Errorf("index %s.%s: column name %s must be identifier", stmt.Table.Name, stmt.Name.Name, col.X)
				}
				if _, ok := tbl.fields[ident.Name]; !ok {
					return nil, fmt.Errorf("index %s.%s: unknown column %s", stmt.Table.Name, stmt.Name.Name, ident.Name)
				}

				if stmt.Unique.IsValid() {
					tbl.uniqueIndicies[stmt.Name.Name] = append(tbl.uniqueIndicies[stmt.Name.Name], ident.Name)
				} else {
					tbl.indicies[stmt.Name.Name] = append(tbl.indicies[stmt.Name.Name], ident.Name)
				}
			}
			tables[stmt.Name.Name] = tbl
		}
	}

	schema := Schema{
		tables: make(map[string]table, len(tables)),
	}
	for _, t := range tables {
		schema.tables[t.name] = t
	}

	return &schema, nil
}

// SQLite is way too lenient on typing spec, for example, the type
// CHARACTER is always equal to TEXT. Why even support the type
// CHARACTER or NCHAR?!
func normalizeType(typ string) string {
	typ = strings.ToUpper(typ)

	if idx := strings.Index(typ, "("); idx != -1 {
		typ = typ[:idx]
	}

	switch typ {
	case "TINYINT", "BOOLEAN":
		typ = "BOOLEAN"
	case "INT", "INTEGER", "SMALLINT", "MEDIUMINT", "BIGINT", "UNSIGNED BIG INT", "INT8", "INT2", "NUMBER":
		typ = "INTEGER"
	case "CHARACTER", "VARCHAR", "VARYING CHARACTER", "NCHAR", "NATIVE CHARACTER", "NVARCHAR", "TEXT", "CLOB":
		typ = "TEXT"
	case "REAL", "DOUBLE", "DOUBLE PRECISION", "FLOAT":
		typ = "REAL"
	case "TIMESTAMP", "DATE", "DATETIME":
		typ = "DATETIME"
	case "NUMERIC", "DECIMAL":
		typ = "NUMERIC"
	}

	return typ
}
