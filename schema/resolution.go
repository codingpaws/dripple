package schema

type ResolutionType string

const (
	ResolutionCreate ResolutionType = "create"
	ResolutionUpdate ResolutionType = "update"
	ResolutionDelete ResolutionType = "delete"
)

type ResolutionOrder int

// Using the ResolutionOrder should get rid of most
// resolution deadlocks, i.e. if we want to drop a
// field but it still has an index mentioning it.
const (
	orderInvalid ResolutionOrder = iota
	OrderTableCreate
	OrderTableAfterCreate
	OrderTableDrop
	OrderColumnCreate
	OrderIndexCreate
	OrderIndexDrop
	OrderIndexUpdate
	OrderColumnDrop
)
