package schema

import (
	"fmt"
	"log"
	"strings"
)

type FieldDef interface {
	PrimaryKey() FieldDef
	Index(name string) FieldDef
	References(table, key string) FieldDef
}

type TableDef interface {
	ToSQL() ([]string, error)
	Name() string
}

const prefixFile = `-- Generated by dripple. DO NOT EDIT.`
const prefixFmt = "-- Table %s\n%s"

func Emit(defs ...TableDef) {
	sqls := []string{}
	for _, v := range defs {
		sql, err := v.ToSQL()
		if err != nil {
			log.Fatalf("encoding %s: %s", v.Name(), err)
		}

		sqls = append(sqls, fmt.Sprintf(prefixFmt, v.Name(), strings.Join(sql, "\n")))
	}
	fmt.Println(prefixFile)
	fmt.Println()
	fmt.Println(strings.Join(sqls, "\n\n"))
}
