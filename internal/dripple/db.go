package dripple

import (
	"database/sql"
	"fmt"
	"os"

	"gitlab.com/codingpaws/dripple/internal/config"
)

func Connect() (*sql.DB, error) {
	url := os.Getenv("DRIPPLE_DB")
	if url == "" {
		return nil, fmt.Errorf("DRIPPLE_DB environment variable must be set")
	}

	driver, err := config.GetDriverFromEnv()
	if err != nil {
		return nil, err
	}

	db, err := sql.Open(driver, url)
	if err != nil {
		return nil, err
	}

	return db, err
}
