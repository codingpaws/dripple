package dripple

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/codingpaws/dripple/pkg/config"
)

func GenerateSchema(conf *config.Config) (rawSchema string, err error) {
	if conf.Schema == "" || !strings.HasSuffix(conf.Schema, ".go") {
		return "", fmt.Errorf("dripple.yml: `schema` must be a go file containing a main()")
	}

	stdout := new(bytes.Buffer)
	stderr := new(bytes.Buffer)

	cmd := exec.Command("go", "run", conf.Schema)
	cmd.Stdout = stdout
	cmd.Stderr = stderr

	err = cmd.Run()
	if err != nil {
		fmt.Println(stdout.String())
		fmt.Println(stderr.String())
		return "", fmt.Errorf("running %s: %w", conf.Schema, err)
	}

	return string(stdout.String()), nil
}

func GenerateAndWriteSchema(conf *config.Config) (sqlPath string, err error) {
	schema, err := GenerateSchema(conf)
	if err != nil {
		return "", err
	}

	sqlPath = conf.Schema + ".sql"

	f, err := os.Create(sqlPath)
	if err != nil {
		return "", fmt.Errorf("opening %s: %w", sqlPath, err)
	}
	defer f.Close()

	_, err = f.Write([]byte(schema))
	if err != nil {
		return "", fmt.Errorf("writing to %s: %w", sqlPath, err)
	}

	if conf.Sqlc {
		out, err := exec.Command("sqlc", "generate").CombinedOutput()
		if err != nil {
			fmt.Println(string(out))
			return "", fmt.Errorf("running sqlc: %w", err)
		}
	}

	return
}
