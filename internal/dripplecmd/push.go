package dripplecmd

import (
	"bytes"
	"database/sql"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/fatih/color"
	"github.com/manifoldco/promptui"
	"github.com/urfave/cli/v2"
	"gitlab.com/codingpaws/dripple/internal/dripple"
	"gitlab.com/codingpaws/dripple/pkg/config"
	"gitlab.com/codingpaws/dripple/schema"
	"gitlab.com/codingpaws/dripple/schema/sqlite"
)

func init() {
	app.Commands = append(app.Commands, &cli.Command{
		Name:   "push",
		Usage:  "Diffs (and optionally uploads) schema to database",
		Action: doPush,
		Flags: []cli.Flag{
			&cli.BoolFlag{Name: "y", Usage: "Pushes the schema to database"},
			&cli.BoolFlag{Name: "f", Aliases: []string{"force"}, Usage: "Allows destructive changes to be pushed"},
		},
	})
}

var faint = color.New(color.Faint)

func doPush(ctx *cli.Context) error {
	if conf == nil {
		return fmt.Errorf("no dripple.yml found, run %s first", color.YellowString("dripple init"))
	}

	noPrompt := ctx.Bool("y")
	destructive := ctx.Bool("force")

	return DoPush(conf, noPrompt, destructive)
}

func DoPush(conf *config.Config, noPrompt, destructive bool) error {
	rawSchemaPath, err := dripple.GenerateAndWriteSchema(conf)
	if err != nil {
		return fmt.Errorf("updating schema: %w", err)
	}
	rawSchema, err := os.ReadFile(rawSchemaPath)
	if err != nil {
		return fmt.Errorf("reading generated schema: %w", err)
	}

	expectedTables, err := sqlite.FromSchema(bytes.NewReader(rawSchema))
	if err != nil {
		return fmt.Errorf("parsing expected schema: %w", err)
	}

	actualTables, db, err := getDatabaseSchema()
	if err != nil {
		return fmt.Errorf("parsing database schema: %w", err)
	}
	defer db.Close()
	tx, err := db.Begin()
	if err != nil {
		return fmt.Errorf("beginning transaction: %w", err)
	}

	resolutions, err := expectedTables.CompareAgainstDatabaseSchema(actualTables)
	if err != nil {
		return fmt.Errorf("generating schema diff: %w", err)
	}

	if len(resolutions) == 0 {
		fmt.Println("Database is up-to-date!")
		return nil
	}

	fmt.Println("Database needs update!")
	fmt.Println()

	var runnableCount int
	if !noPrompt {
		runnableCount, err = runResolutions(nil, resolutions, true, destructive)
		if err != nil {
			return err
		}

		if runnableCount == 0 && !destructive {
			fmt.Println("Destructive changes detected!")
			fmt.Printf("Use %s to push destructive changes.\n", color.YellowString("-f"))
			return nil
		}
	}

	if noPrompt || prompt(runnableCount) {
		runnableCount, err = runResolutions(tx, resolutions, false, destructive)
		if err != nil {
			return fmt.Errorf("pushing resolutions: %w", err)
		}
		err = tx.Commit()
		if err != nil {
			return fmt.Errorf("pushing changes: %w", err)
		}
		if runnableCount > 0 {
			fmt.Println("Pushed successfully!")
		} else {
			faint.Print("Skipped pushing! ")
			fmt.Printf("Use %s to apply destructive changes.\n", color.YellowString("-f"))
		}
	}

	return nil
}

func runResolutions(tx *sql.Tx, resolutions []sqlite.Resolution, dryRun bool, destructive bool) (runnableCount int, err error) {
	if dryRun {
		fmt.Println("Resolution steps:")
	} else {
		fmt.Println("Pushing resolutions:")
	}

	for i, v := range resolutions {
		var label string
		switch v.Type {
		case schema.ResolutionCreate:
			label = color.GreenString("new")
		case schema.ResolutionUpdate:
			label = color.YellowString("chg")
		case schema.ResolutionDelete:
			label = color.RedString("del")
		}
		if v.Destructive {
			v.Description = "🔥 " + v.Description
		}
		fmt.Printf("  %s [%s] %s", faint.Sprintf("%d.", i+1), label, v.Description)
		if (destructive && v.Destructive) || !v.Destructive {
			runnableCount++
		}
		if dryRun && v.Destructive {
			fmt.Println()
			continue
		}
		if !dryRun && !destructive && v.Destructive {
			faint.Println(" (skipped)")
			continue
		}
		fmt.Println()
		if !dryRun {
			for _, sql := range v.Queries {
				prettySql := strings.Split(sql, "\n")
				for i, v := range prettySql {
					if i > 0 {
						prettySql[i] = "      " + v
					}
				}
				fmt.Printf("    - %s\n", faint.Sprint(strings.TrimSpace(strings.Join(prettySql, "\n"))))
				_, err := tx.Exec(sql)
				if err != nil {
					return 0, fmt.Errorf("executing step %d: %w", i+1, err)
				}
			}
		}
	}

	fmt.Println()

	return
}

func getDatabaseSchema() (schema *sqlite.Schema, db *sql.DB, err error) {
	db, err = dripple.Connect()
	if err != nil {
		return nil, nil, fmt.Errorf("opening database connection: %w", err)
	}
	defer func() {
		if err != nil && db != nil {
			db.Close()
		}
	}()

	rows, err := db.Query("select sql from sqlite_schema")
	if err != nil {
		return nil, nil, fmt.Errorf("getting schema: %w", err)
	}

	b := strings.Builder{}

	for rows.Next() {
		var sql sql.NullString
		err = rows.Scan(&sql)
		if err != nil {
			return nil, nil, err
		}
		if sql.Valid {
			_, _ = b.WriteString(sql.String)
			if !strings.HasSuffix(sql.String, ";") {
				_ = b.WriteByte(';')
			}
			_ = b.WriteByte('\n')
		}
	}

	schema, err = sqlite.FromSchema(strings.NewReader(b.String()))
	if err != nil {
		fmt.Println(b.String())
		return nil, nil, err
	}

	return
}

func prompt(runnableCount int) bool {
	prompt := promptui.Select{
		Label:        fmt.Sprintf("Push %d resolutions?", runnableCount),
		HideHelp:     true,
		Items:        []string{"No", "Yes"},
		HideSelected: true,
	}
	_, result, err := prompt.Run()
	if err != nil {
		log.Fatalf("Prompt failed %v\n", err)
	}
	return result == "Yes"
}
