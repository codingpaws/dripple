package dripplecmd

import (
	"fmt"

	"github.com/urfave/cli/v2"
)

func init() {
	app.Commands = append(app.Commands, &cli.Command{
		Name:   "pull",
		Usage:  "Pulls the database schema and prints a potential Go schema.go",
		Action: doPull,
		Flags: []cli.Flag{
			&cli.BoolFlag{Name: "y", Usage: "Pulls the schema without asking"},
		},
	})
}

func doPull(ctx *cli.Context) error {
	schema, db, err := getDatabaseSchema()
	if err != nil {
		return fmt.Errorf("parsing database schema: %w", err)
	}
	_ = db.Close()

	goCode, err := schema.ToGo()
	if err != nil {
		return fmt.Errorf("serializing schema: %w", err)
	}

	fmt.Println(goCode)

	return nil
}
