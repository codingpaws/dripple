package dripplecmd

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/fatih/color"
	"github.com/manifoldco/promptui"
	"github.com/urfave/cli/v2"
	"gitlab.com/codingpaws/dripple/internal/dripple"
	"gitlab.com/codingpaws/dripple/pkg/config"
	"gopkg.in/yaml.v3"
)

func init() {
	app.Commands = append(app.Commands, &cli.Command{
		Name:   "init",
		Usage:  "Initializes a dripple app",
		Action: runInit,
	})
}

const exampleSchema = `package main

import (
	"gitlab.com/codingpaws/dripple/schema"
	"gitlab.com/codingpaws/dripple/schema/sqlite"
)

var articles = sqlite.Table(
	"articles",

	sqlite.Integer("id").PrimaryKey(),
	sqlite.String("title"),
	sqlite.String("author").Index("author"),
)


func main() {
	schema.Emit(
		articles,
	)
}
`

const exampleSqlc = `version: "2"
sql:
  - engine: "sqlite"
    queries: "{path}/queries"
    schema: "{path}/schema/schema.go.sql"
    gen:
      go:
        package: "gen"
        out: "{path}/gen"
`

const exampleQuery = `-- name: GetArticles :many
select
  *
from
  articles;
`

func runInit(ctx *cli.Context) error {
	_, err := os.Stat("dripple.yml")
	if err == nil {
		return fmt.Errorf("dripple.yml already exists")
	} else if !os.IsNotExist(err) {
		fmt.Printf("%#v\n", err)
		return err
	}

	_, schemaDir, err := (&promptui.Select{
		Label: "Where do you want your schema to live?",
		Items: []string{"internal/store/schema/ (recommended)", "store/schema/"},
	}).Run()
	if err != nil {
		return err
	}
	schemaDir = strings.SplitN(schemaDir, " ", 2)[0]
	schemaDir = strings.TrimRight(schemaDir, "/")

	_, useSqlc, err := (&promptui.Select{
		Label: "Do you want to use sqlc for queries?",
		Items: []string{"Yes", "No"},
	}).Run()
	if err != nil {
		return err
	}

	conf := config.Config{
		Schema: filepath.Join(schemaDir, "schema.go"),
		Sqlc:   useSqlc == "Yes",
	}

	if conf.Sqlc {
		err = exec.Command("sqlc", "version").Run()
		if errors.Is(err, exec.ErrNotFound) {
			_, installSqlc, err := (&promptui.Select{
				Label: "sqlc not found. Do you want to install sqlc?",
				Items: []string{"Yes", "No"},
			}).Run()
			if err != nil {
				return err
			}
			if installSqlc == "No" {
				return fmt.Errorf("sqlc is not installed")
			}
			output, err := exec.Command("go", "install", "github.com/sqlc-dev/sqlc/cmd/sqlc@latest").CombinedOutput()
			if err != nil {
				fmt.Println(string(output))
				return fmt.Errorf("trying to install sqlc: %w", err)
			}
		} else if err != nil {
			return err
		}
	}

	b, err := exec.Command("go", "get", "gitlab.com/codingpaws/dripple").CombinedOutput()
	if err != nil {
		fmt.Println(string(b))
		return fmt.Errorf("downloading dripple dependency: %w", err)
	}
	b, err = exec.Command("go", "get", "gitlab.com/codingpaws/dripple/schema").CombinedOutput()
	if err != nil {
		fmt.Println(string(b))
		return fmt.Errorf("downloading dripple/schema dependency: %w", err)
	}

	f, err := os.Create("dripple.yml")
	if err != nil {
		return err
	}
	defer f.Close()
	err = yaml.NewEncoder(f).Encode(conf)
	if err != nil {
		return err
	}

	err = os.MkdirAll(schemaDir, 0777)
	if err != nil {
		return fmt.Errorf("creating schema dir %s: %w", schemaDir, err)
	}

	schemaF, err := os.Create(conf.Schema)
	if err != nil {
		return fmt.Errorf("creating example schema file: %w", err)
	}
	defer schemaF.Close()

	schemaF.Write([]byte(exampleSchema))

	if conf.Sqlc {
		sqlcF, err := os.Create("sqlc.yaml")
		if err != nil {
			return fmt.Errorf("creating sqlc config: %w", err)
		}
		defer sqlcF.Close()

		_, err = fmt.Fprint(sqlcF, strings.ReplaceAll(exampleSqlc, "{path}", filepath.Dir(schemaDir)))
		if err != nil {
			return fmt.Errorf("writing sqlc.yaml: %w", err)
		}

		queryDir := filepath.Join(filepath.Dir(schemaDir), "queries")

		err = os.MkdirAll(queryDir, 0777)
		if err != nil {
			return fmt.Errorf("creating queries dir %s: %w", queryDir, err)
		}

		queryF, err := os.Create(filepath.Join(queryDir, "articles.sql"))
		if err != nil {
			return err
		}
		defer queryF.Close()

		_, err = fmt.Fprint(queryF, exampleQuery)
		if err != nil {
			return fmt.Errorf("writing %s/articles.sql: %w", queryDir, err)
		}
	}

	_, err = dripple.GenerateAndWriteSchema(&conf)
	if err != nil {
		return err
	}

	dripple := color.MagentaString("dripple")
	faint := color.New(color.Faint)
	blue := color.BlueString
	yellow := color.YellowString
	fmt.Printf("Initialized %s project successfully!\n", dripple)
	fmt.Println()
	fmt.Printf("What %s generated for you:\n", dripple)
	fmt.Println()
	fmt.Printf("  %s %s\n", faint.Sprint("1."), blue("The config file at %s", yellow("dripple.yml")))
	fmt.Printf("  %s %s\n", faint.Sprint("2."), blue("Your schema at %s", yellow(conf.Schema)))
	fmt.Printf("  %s %s\n", faint.Sprint("3."), blue("The auto-generated SQL schema at %s", yellow(conf.Schema+".sql")))

	if conf.Sqlc {
		fmt.Println()
		fmt.Printf("  %s %s\n", faint.Sprint("4."), blue("The sqlc config at %s", yellow("sqlc.yaml")))
		fmt.Printf("  %s %s\n", faint.Sprint("5."), blue("An example sqlc query at %s", yellow(filepath.Join(schemaDir, "..", "queries", "articles.sql"))))
		fmt.Printf("  %s %s\n", faint.Sprint("6."), blue("The sqlc generated files in %s", yellow(filepath.Join(schemaDir, "..", "gen"))))
	}

	fmt.Println()

	fmt.Printf("The %s CLI takes the database from the DRIPPLE_DB environment\n", dripple)
	fmt.Printf("variable. To use sqlite, use e.g. %s. To use libSQL,\n", blue("file:mydatabase.db"))
	fmt.Printf("use the %s URL you got from your database provider (e.g. Turso).\n", blue("sqlite://"))

	fmt.Println()

	fmt.Printf("%s if you use the DRIPPLE_DB env var in production, you can create\n", faint.Sprint("Tip:"))
	fmt.Printf("a database connection by using dripple.Connect() in your code.\n")

	fmt.Println()

	fmt.Printf("Run %s to generate the SQL schema from schema.go.\n", yellow("dripple generate"))
	fmt.Printf("Run %s to push schema changes to your database.\n", yellow("dripple push"))

	return nil
}
