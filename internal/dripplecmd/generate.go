package dripplecmd

import (
	"fmt"
	"time"

	"github.com/fatih/color"
	"github.com/urfave/cli/v2"
	"gitlab.com/codingpaws/dripple/internal/dripple"
)

func init() {
	app.Commands = append(app.Commands, &cli.Command{
		Name:   "generate",
		Usage:  "Generates schema file",
		Action: runGenerate,
	})
}

func runGenerate(ctx *cli.Context) error {
	if conf == nil {
		return fmt.Errorf("no dripple.yml found, run %s first", color.YellowString("dripple init"))
	}
	start := time.Now()

	sqlPath, err := dripple.GenerateAndWriteSchema(conf)
	if err != nil {
		return err
	}

	fmt.Printf(
		"Generated %s in %s.\n",
		color.New(color.Faint).Sprint(sqlPath),
		color.YellowString("%dms", time.Since(start).Milliseconds()),
	)

	return nil
}
