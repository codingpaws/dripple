package dripplecmd

import (
	"fmt"

	"github.com/fatih/color"
	_ "github.com/tursodatabase/libsql-client-go/libsql"
	"github.com/urfave/cli/v2"
	"gitlab.com/codingpaws/dripple/internal/dripple"
	_ "modernc.org/sqlite"
)

func init() {
	app.Commands = append(app.Commands, &cli.Command{
		Name:   "test",
		Usage:  "Tests the database connection",
		Action: runTest,
	})
}

func runTest(ctx *cli.Context) error {
	if conf == nil {
		return fmt.Errorf("no dripple.yml found, run %s first", color.YellowString("dripple init"))
	}

	db, err := dripple.Connect()
	if err != nil {
		return fmt.Errorf("opening database: %w", err)
	}
	res, err := db.Query("select 1")
	if err != nil {
		return err
	}
	defer res.Close()
	var val int
	if !res.Next() {
		return fmt.Errorf("expected one row from database test but got none")
	}
	err = res.Scan(&val)
	if err != nil {
		return err
	}
	if val != 1 {
		return fmt.Errorf("database connection returned unexpected data")
	}
	fmt.Println("Connection test successful.")

	return nil
}
