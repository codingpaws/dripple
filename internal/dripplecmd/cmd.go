package dripplecmd

import (
	"log"
	"os"

	"github.com/fatih/color"
	"github.com/joho/godotenv"
	"gitlab.com/codingpaws/dripple/internal/cliutil"
	"gitlab.com/codingpaws/dripple/pkg/config"
)

var conf *config.Config
var app = cliutil.NewApp()

func Run() {
	godotenv.Load()
	var err error

	conf, err = config.Load()
	if err != nil {
		log.Fatalf("loading config at dripple.yml: %s", err)
	}
	app.Name = color.MagentaString("dripple")
	app.Usage = "Database schemas in Go, made simple"

	err = app.Run(os.Args)
	if err != nil {
		log.Fatalln(err)
	}
}
