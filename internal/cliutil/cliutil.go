package cliutil

import "github.com/urfave/cli/v2"

func NewApp() *cli.App {
	app := cli.NewApp()
	app.CustomAppHelpTemplate = helpTemplate
	return app
}
