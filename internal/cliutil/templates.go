package cliutil

import (
	"fmt"

	"github.com/fatih/color"
)

var helpTemplate = fmt.Sprintf(`{{template "helpNameTemplate" .}}

%s:
   {{if .UsageText}}{{wrap .UsageText 3}}{{else}}{{.HelpName}} {{if .VisibleFlags}}[global options]{{end}}{{if .Commands}} command [command options]{{end}} {{if .ArgsUsage}}{{.ArgsUsage}}{{else}}{{if .Args}}[arguments...]{{end}}{{end}}{{end}}{{if .Version}}{{if not .HideVersion}}

%s:
   {{.Version}}{{end}}{{end}}{{if .Description}}

%s:
   {{template "descriptionTemplate" .}}{{end}}
{{- if len .Authors}}

%s{{template "authorsTemplate" .}}{{end}}{{if .VisibleCommands}}

%s:{{template "visibleCommandCategoryTemplate" .}}{{end}}{{if .VisibleFlagCategories}}

%s:{{template "visibleFlagCategoryTemplate" .}}{{else if .VisibleFlags}}

%s:{{template "visibleFlagTemplate" .}}{{end}}{{if .Copyright}}

%s:
   {{template "copyrightTemplate" .}}{{end}}
`, color.YellowString("Usage"), color.YellowString("Version"), color.YellowString("Description"), color.YellowString("Author"), color.YellowString("Commands"), color.YellowString("Global options"), color.YellowString("Global options"), color.YellowString("Copyright"))
