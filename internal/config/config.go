package config

import (
	"fmt"
	"os"
	"strings"
)

func GetDriverFromEnv() (string, error) {
	url := os.Getenv("DRIPPLE_DB")

	if strings.HasPrefix(url, "file:") {
		return "sqlite", nil
	} else if strings.HasPrefix(url, "libsql://") {
		return "libsql", nil
	}

	return "", fmt.Errorf("can’t determine driver from DRIPPLE_DB environment variable, use prefix 'file:' for sqlite3 or 'libsql://' for libsql")
}
