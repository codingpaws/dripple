package main

import (
	"gitlab.com/codingpaws/dripple/schema"
	"gitlab.com/codingpaws/dripple/schema/sqlite"
)

var articles = sqlite.Table(
	"articles",

	sqlite.Integer("id").PrimaryKey(),
	sqlite.String("title"),
)

var users = sqlite.Table(
	"users",

	sqlite.Integer("id").PrimaryKey(),
	sqlite.String("name"),
	sqlite.Bool("is_admin").Default("false"),
	sqlite.String("email").Unique().Index("email"),
)

var customers = sqlite.Table(
	"customers",

	sqlite.Integer("id").PrimaryKey(),
	sqlite.String("name").UniqueIndex("name_contact"),
	sqlite.String("contact_name").UniqueIndex("name_contact"),
	sqlite.Datetime("created_at").Default("(strftime('%Y-%m-%dT%H:%M:%fZ', 'now'))"),
)

var categories = sqlite.Table(
	"categories",

	sqlite.Integer("id").PrimaryKey(),
	sqlite.String("creator_id").Index("creator_id"),
)

func main() {
	schema.Emit(
		articles,
		users,
		categories,
		customers,
	)
}
