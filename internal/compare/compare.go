package compare

import "reflect"

// Compares two maps and returns true as early as possible if there is a
// difference in keys or if one of the value doesn’t reflect.DeepEqual().
func IsDifferent[K comparable, V any](existing, newer map[K]V) bool {
	for k, newerValue := range newer {
		existingValue, ok := existing[k]

		if !ok {
			return true
		} else if !reflect.DeepEqual(existingValue, newerValue) {
			return true
		}
	}

	for k := range existing {
		_, ok := newer[k]
		if !ok {
			return true
		}
	}

	return false
}

// Reports the changes in the `newer` map compared to the `existing` map.
//
// The results are each a list of keys:
//   - `added`: included in `newer` but not in `existing`
//   - `deleted`: not included in `newer` but in `existing`
//   - `changed`: in both maps but the values don’t reflect.DeepEqual()
func CompareMap[K comparable, V any](existing, newer map[K]V) (added []K, deleted []K, changed []K) {
	for k, newerValue := range newer {
		existingValue, ok := existing[k]

		if !ok {
			added = append(added, k)
		} else if !reflect.DeepEqual(existingValue, newerValue) {
			changed = append(changed, k)
		}
	}

	for k := range existing {
		_, ok := newer[k]
		if !ok {
			deleted = append(deleted, k)
		}
	}

	return
}
