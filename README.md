# Dripple

Dripple is a simple SQLite/libSQL schema tool, which allows you to define
a schema in Go code and push it to your database. Pushing is a modern way
that automates database migrations by creating, updating, and deleting
tables/fields/indicies for you when you run `dripple push`.

If you choose to use dripple with [sqlc][sqlc], you can define your
schema and write type-safe queries.

[sqlc]: https://sqlc.dev

## Getting started

To start using dripple, you need to have Go (> 1.20) installed.

```sh
go install gitlab.com/codingpaws/dripple/cmd/dripple@latest
```

Now, go to your project directory and run `dripple init`. This will
generate the `dripple.yml` config and a schema file (and optionally set
up sqlc) at `store/schema/schema.go`. You can freely move this file but
you need to update its path in `dripple.yml`.

## Why the name?

Dripple is obviously inspired by Drizzle’s schema push system. Drizzle in
German (my native langauge) translates to “nieseln,” however in Northern
Germany we sometimes say “drippeln” instead of “nieseln.” Since dripple
is a far simpler tool (no ORM, just a schema helper), it felt like a
funny and fitting name.
