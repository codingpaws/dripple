package main

import (
	"gitlab.com/codingpaws/dripple/internal/dripplecmd"
)

func main() {
	dripplecmd.Run()
}
