package dripple

import (
	"gitlab.com/codingpaws/dripple/internal/dripplecmd"
	"gitlab.com/codingpaws/dripple/pkg/config"
)

type PushOptions struct {
	Destructive bool
}

type PushResult struct{}

// Perform push programmatically based on dripple.yml.
//
// PushResult is always nil, it’s part of the API to
// be future-proof and communicate changes outside.
func Push(conf *config.Config, opt *PushOptions) (*PushResult, error) {
	if opt == nil {
		opt = &PushOptions{}
	}

	return nil, dripplecmd.DoPush(conf, true, opt.Destructive)
}
